import "bootstrap/dist/css/bootstrap.min.css";
import { Route, Routes } from "react-router-dom";
import Home from "./Pages/Home/Home";
import MainHeader from "./Components/Headers/Main.Header";
import "./App.css";
import Footer from "./Components/Footer/Footer";
import ProductDetail from "./Pages/Detail/ProductDetail";
import TreadingPage from "./Pages/Treading/Treading";
import MoviesPage from "./Pages/Movies/Movies";
import { useEffect } from "react";
import WebFont from "webfontloader";
import TvSeriesPage from "./Pages/Tv-Series/TvSeries";

function App() {
  useEffect(() => {
    WebFont.load({
      google: {
        families: ['Droid Sans', 'Julius Sans One', "Bungee",]
      }
    });
  }, [])
  return (
    <>
      <MainHeader />
      <div className="App">
        <Routes>
          <Route path="/" element={<Home />} />
          <Route path="/Detail/:mediaType/:id" element={<ProductDetail />} />
          <Route path="/Treading" element={<TreadingPage />} />
          <Route path="/Treading/page/:page" element={<TreadingPage />}/>
          {/* movies */}
          <Route path="/all-Movies" element={<MoviesPage />} />
          <Route path="/movie/categories/:media/:id/page/:page" element={<MoviesPage />} />
          <Route path="/movie/categories/:media/page/:page" element={<MoviesPage />} />
          <Route path="/movie/:searchFilm/page/:page" element={<MoviesPage />} />
          <Route path="/movie/page/:page" element={<MoviesPage />} />
          {/* Tv-series */}
          <Route path="/all-Series" element={<TvSeriesPage />} />
          <Route path="/series/categories/:media/:id/page/:page" element={<TvSeriesPage />} />
          <Route path="/series/categories/:media/page/:page" element={<TvSeriesPage />} />
          <Route path="/series/:searchFilm/page/:page" element={<TvSeriesPage />} />
          <Route path="/series/page/:page" element={<TvSeriesPage />} />
        </Routes>
      </div>
      <Footer />
    </>
  );
}

export default App;
