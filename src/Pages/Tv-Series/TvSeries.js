import axios from "axios";
import { useEffect, useState } from "react";
import SearchFilm from "../../Components/Search/Search";
import FilterGenre from "../../Components/FilterGenre/FilterGenre";
import PaginationMovies from "../../Components/Pagination/PaginationMovies";
import ProductMovies from "../../Components/ProductMovies/ProductMovies";


const TvSeriesPage = () => {
    const [movies, setMovies] = useState([]);
    const [isLoading, setLoading] = useState(false);
    const [page, setPage] = useState(1);
    const [genreTitle, setGenreTitle] = useState();
    const [genreFilter, setGenreFilter] = useState([]);
    const [searchFilm, setSearchFilm] = useState("");
    const [numberPage, setNumberPage] = useState();
    // call api get tv-series with genre filter
    const fetchGetApiAllMovies = async () => {
        try {
            const { data } = await axios.get(
                `https://api.themoviedb.org/3/discover/tv?api_key=39f5897aa2b8f37692fc06e61504587d&page=${page}&sort_by=popularity.desc&with_genres=${genreFilter.id}`
            );
            const MoviesData = data.results;
            console.log("MoviesData", MoviesData)
            console.log("toltal page", data.total_pages)
            setMovies(MoviesData)
            setNumberPage(data.total_pages > 100 ? 100 : data.total_pages )
            setLoading(true)
        }
        catch (error) {
            console.error(error)
        }
    }
    console.log("genreFilterId", genreFilter.id)
    const fetchGetApiSearchFilm = async () => {
        if (searchFilm) {
            const { data } = await axios.get(
                `https://api.themoviedb.org/3/search/tv?api_key=39f5897aa2b8f37692fc06e61504587d&query=${searchFilm}&page=${page}&sort_by=popularity.desc`
            );
            const MoviesData = data.results;
            setNumberPage(data.total_pages > 100 ? 100 : data.total_pages )
            console.log("MovieSearch", MoviesData)
            setMovies(MoviesData)
            setLoading(true)
        }
    }
    console.log("genreFilter", genreFilter)
    console.log("search film", searchFilm)
    console.log("genreTitle",genreTitle)
    useEffect(() => {
        if(searchFilm){
            fetchGetApiSearchFilm();
        }else{
            fetchGetApiAllMovies();
        }
       
    }, [genreFilter,page, isLoading, searchFilm])
    const handlerChangePage = (page) => {
        setPage(page)
    }
    const handGenreTitle = (genre) => {
        setGenreTitle(genre);
    }
    const handGenreFilter = (genreFilter) => {
        setGenreFilter(genreFilter)
    }
   
    console.log("page", page)

    return (
        <>
            <div className="container treading-movie">
                <div className="tilte-movie">
                    <div className="btn-movies">
                        <h3 style={{ color: "#fff", fontFamily: "Bungee" }}>{genreTitle ? genreTitle.name : ""} Tv Series:</h3>
                    </div>

                    <div className="search">
                        <SearchFilm setSearchFilm = {setSearchFilm} searchFilm = {searchFilm} setPage={handlerChangePage} fetchGetApiSearchFilm={fetchGetApiSearchFilm}/>
                    </div>

                </div>
                <div className="filter-genre">
                    <FilterGenre media="tv" genreTitle={handGenreTitle} genreFilter={handGenreFilter} setPage={handlerChangePage} setSearchFilm = {setSearchFilm} />
                </div>
                <div className="list-treading">
                    {isLoading && movies.length > 0 ? (movies.map((data) => (
                        <ProductMovies key={data.id} {...data} mediaType = "tv"/>
                    ))) : <p style={{
                        color: "grey",
                        fontSize: "30px",
                        marginLeft: "10px",
                        marginTop: "10px",
                    }}>No have result</p>}
                </div>
                <div className="pagination">
                    {movies && movies.length > 0 ? (<PaginationMovies setPage={handlerChangePage} numberPage = {numberPage} media = {"series"} genreFilter = {genreFilter} searchFilm = {searchFilm} genreTitle = {genreTitle}/>)
                    : null}
                    
                </div>
            </div>
        </>
    )
}
export default TvSeriesPage