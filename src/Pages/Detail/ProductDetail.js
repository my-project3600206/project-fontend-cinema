import axios from "axios";
import { useEffect, useState } from "react";
import { useParams } from "react-router-dom"
import { image_300, unavailable } from "../../Api/config/DefaultConfig";
import "../../Pages/Detail/ProductDetail.css"
import VideoTrailer from "../../Components/VideoTrailer/VideoTrailer";
import Cast from "../../Components/CarouselCast/CarouselCast";
import ProductMovies from "../../Components/ProductMovies/ProductMovies";
// import "../../Components/ProductMovies/ProductMovies.css";


const ProductDetail = () => {
    const { id, mediaType } = useParams();
    const [dataMovies, setDataMovies] = useState();
    const [isLoading, setLoading] = useState(false);
    const [videoTrailer, setVideoTrailer] = useState();
    const [similar, setSimilar]= useState()
    console.log("id", id)
    //get info of movie by movie id
    const fetchData = async () => {
        try {
            const { data } = await axios.get(`https://api.themoviedb.org/3/${mediaType}/${id}?api_key=39f5897aa2b8f37692fc06e61504587d`);
            console.log("data", data)
            setDataMovies(data);
            setLoading(true);
        } catch (error) {
            console.error(error)
        }
    };
     //get similar movie
     const fetchSimlarData = async () => {
        try {
            const { data } = await axios.get(`https://api.themoviedb.org/3/${mediaType}/${id}/similar?api_key=39f5897aa2b8f37692fc06e61504587d`);
            console.log("data", data)
            const dataSimilar = data.results.slice(0,7);
            setSimilar(dataSimilar);
            setLoading(true);
        } catch (error) {
            console.error(error)
        }
    };
    //get video of movie current
    const fetchGetDataVideoTrailer = async () => {
        try {
            const { data } = await axios.get(`https://api.themoviedb.org/3/${mediaType}/${id}/videos?api_key=39f5897aa2b8f37692fc06e61504587d`);
            console.log("datavideo", data)
            if (data.results[0].key) {
                setVideoTrailer(data.results[0].key);
            } else {
                console.log("No have video")
            }

        } catch (error) {
            console.error(error)
        }
    };
    useEffect(() => {
        fetchData();
        fetchGetDataVideoTrailer();
        fetchSimlarData();
    }, [id, mediaType])
    return (
        <>
            <div>
                {dataMovies && (
                    <div className="detail-page"
                        style={{
                            backgroundImage: `url(https://www.themoviedb.org/t/p/w1920_and_h800_multi_faces/${dataMovies.backdrop_path})`,
                        }}>

                        <img
                            className="image-poster"
                            src={dataMovies.poster_path ? `${image_300}/${dataMovies.poster_path}` : unavailable}
                            alt=""
                        />

                        <div className="detail-page-info">
                            <h3>{dataMovies.original_title || dataMovies.name}</h3>
                            {/* get year */}
                            <div className="year">
                                <b>{(dataMovies.first_air_date || dataMovies.release_date).substring(0, 4)}</b>
                                .
                                <b className="title">{mediaType === "tv" ? "Tv Series ." : "Movie ."}</b>
                                <b className="vote">
                                    <b className="tmdb">TMDB</b>
                                    <b className="vote_ave">-⭐{+dataMovies.vote_average.toFixed(1)}</b>
                                </b>
                            </div>
                            <div className="genres">
                                {dataMovies.genres.map((item, i) => (
                                    <p key={item.id} className="genresItem">
                                        {i > 0 && ", "}
                                        {item.name}
                                    </p>
                                ))}

                            </div>
                            <div className="video-trailer">
                                <VideoTrailer keyTrailer={videoTrailer} title={dataMovies.original_title || dataMovies.name} />
                            </div>
                            <div className="tagline">
                                <h5>{dataMovies.tagline}</h5>
                            </div>
                            <div className="overview">
                                <h6>{dataMovies.overview}</h6>
                            </div>
                            <div className="other_lists">
                                <ul style={{ padding: 0 }}>
                                    <li>
                                        DURATION:{" "}
                                        <span>
                                            {mediaType === "tv"
                                                ? `${dataMovies.episode_run_time[0]} min episodes`
                                                : `${dataMovies.runtime} min`}
                                        </span>
                                    </li>
                                    {mediaType === "tv" ? (
                                        <li>
                                            SEASONS: <span>{dataMovies.number_of_seasons}</span>
                                        </li>
                                    ) : (
                                        ""
                                    )}

                                    <li>
                                        STUDIO:
                                        {dataMovies.production_companies.map((studio, i) => {
                                            return (
                                                <span key={studio.id}>
                                                    {" "}
                                                    {i > 0 && ",  "}
                                                    {studio.name}
                                                </span>
                                            );
                                        })}
                                    </li>
                                    {mediaType === "movie" ? (
                                        <li>
                                            RELEASE DATE: <span>{dataMovies.release_date}</span>
                                        </li>
                                    ) : (
                                        ""
                                    )}
                                    <li>
                                        STATUS: <span>{dataMovies.status}</span>
                                    </li>
                                </ul>
                            </div>
                        </div>

                    </div>)}


            </div>
            <div className="all-cast">
                <div className="cast_title">
                    <h4>Cast</h4>
                </div>
                <div>
                    <Cast mediaType={mediaType} id={id} />
                </div>
            </div>
            <div className="similar_shows">
                <div className="similar_title">
                    <h4>You May Also Like </h4>
                </div>
                <div className="similar">
                    {similar &&
                        similar.map((data) => (
                            <ProductMovies key={data.id} {...data} mediaType="movie"/>
                        ))}
                </div>
            </div>

        </>
    )
}
export default ProductDetail