import axios from "axios";
import { useEffect, useState } from "react";
import ProductMovies from "../../Components/ProductMovies/ProductMovies";
import "../../Pages/Treading/Treading.css"
import PaginationMovies from "../../Components/Pagination/PaginationMovies";


const TreadingPage = () => {
    const [treading, setTreading] = useState([]);
    const [isLoading, setLoading] = useState(false);
    const [page, setPage] = useState(1)
    // call api get all data treading show
    const fetchGetApiAllTreading = async () => {
        try {
            const { data } = await axios.get(
                `https://api.themoviedb.org/3/trending/all/day?api_key=39f5897aa2b8f37692fc06e61504587d&page=${page}`
            );
            const TreadingData = data.results;
            console.log("TreadingData", TreadingData)
            const filter = TreadingData;
            setTreading(filter)
        }
        catch (error) {
            console.error(error)
        }
    }
    const handlerChangePage = (page)=>{
        setPage(page)
    }
    console.log("papage", page)
    useEffect(()=>{
        fetchGetApiAllTreading();
    },[page])

    return (
        <>
            <div className="container treading-movie">
                <div className="tilte-movie">
                    <div className="btn-movies">
                        <h5>
                            TREADING SHOW &#160;
                            <span style={{ paddingTop: "10px" }}>&#11166;</span>
                        </h5>
                    </div>
                    
                </div>
                <div className="list-treading">
                    {treading.map((data) => (
                        <ProductMovies key={data.id} {...data} />
                    ))}
                </div>
                <div className="pagination">
                    <PaginationMovies setPage = {handlerChangePage} numberPage = {5} media={"Treading"}/>
                </div>
            </div>
        </>
    )
}
export default TreadingPage