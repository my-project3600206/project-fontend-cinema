import { Link } from "react-router-dom"
import CarouselHomePage from "../../Components/Carousel/CarouselHomePage"
import "../../Pages/Home/Home.css"
import axios from "axios"
import { useEffect, useState } from "react"
import ProductMovies from "../../Components/ProductMovies/ProductMovies"
const Home = () => {
    const [resMovies, setMovies] = useState([])
    const [resSeries, setSeries] = useState([])
    const [resTopRated, setTopRated] = useState([])
    // call api get data movies on air
    const fetchGetApiMovies = async () => {
        try {
            const { data } = await axios.get(
                `https://api.themoviedb.org/3/movie/now_playing?api_key=39f5897aa2b8f37692fc06e61504587d`,
            );
            const allData = data.results;
            console.log("allData", allData)
            const filter = allData.slice(0, 7);
            setMovies(filter)
        }
        catch (error) {
            console.error(error)
        }
    }
    // call api get data tvseries on air
    const fetchGetApiTvSeries = async () => {
        try {
            const { data } = await axios.get(
                `https://api.themoviedb.org/3/tv/on_the_air?api_key=39f5897aa2b8f37692fc06e61504587d`,
            );
            const allData = data.results;
            console.log("allData", allData)
            const filter = allData.slice(0, 7);
            setSeries(filter)
        }
        catch (error) {
            console.error(error)
        }
    }
    // call api get data top rated
    const fetchGetApiTopRated = async () => {
        try {
            const { data } = await axios.get(
                `https://api.themoviedb.org/3/movie/top_rated?api_key=39f5897aa2b8f37692fc06e61504587d`,
            );
            const allData = data.results;
            console.log("allData", allData)
            const filter = allData.slice(0, 7);
            setTopRated(filter)
           
        }
        catch (error) {
            console.error(error)
        }
    }

    useEffect(() => {
        fetchGetApiMovies();
        fetchGetApiTvSeries();
        fetchGetApiTopRated();
    }, [])
    return (
        <div className="wapper-home">
            <div className="carousel" >
                <CarouselHomePage />
            </div>
            <div className="container content-movie">
                <div className="tilte-movie">
                    <div className="btn-movies">
                        <h5>
                            MOVIES ON AIR &#160;
                            <span style={{ paddingTop: "10px" }}>&#11166;</span>
                        </h5>
                    </div>
                    <div className="view-more">
                        <Link to="/all-Movies" style={{ textDecoration: "none" }} >
                            <p>VIEW MORE &#187;</p>
                        </Link>
                    </div>
                </div>
                <div className="list-content">
                    {resMovies.map((data) => (
                        <ProductMovies key={data.id} {...data} mediaType="movie" />
                    ))}
                </div>
            </div>
            <hr className="hr-element"/>
            <div className="container content-movie">
                <div className="tilte-movie">
                    <div className="btn-movies">
                        <h5>
                            TVSERIES ON AIR &#160;
                            <span style={{ paddingTop: "10px" }}>&#11166;</span>
                        </h5>
                    </div>
                    <div className="view-more">
                        <Link to="/all-Series" style={{ textDecoration: "none" }} >
                            <p>VIEW MORE &#187;</p>
                        </Link>
                    </div>
                </div>
                <div className="list-content">
                    {resSeries.map((data) => (
                        <ProductMovies key={data.id} {...data} mediaType="tv" />
                    ))}
                </div>
            </div>
            <hr className="hr-element"/>
            <div className="container content-movie">
                <div className="tilte-movie">
                    <div className="btn-movies">
                        <h5>
                            TOP RATED &#160;
                            <span style={{ paddingTop: "10px" }}>&#11166;</span>
                        </h5>
                    </div>
                    <div className="view-more">
                        <Link to="/all-Movies" style={{ textDecoration: "none" }} >
                            <p>VIEW MORE &#187;</p>
                        </Link>
                    </div>
                </div>
                <div className="list-content">
                    {resTopRated.map((data) => (
                        <ProductMovies key={data.id} {...data} mediaType="movie" />
                    ))}
                </div>
            </div>

        </div>
    )
}
export default Home