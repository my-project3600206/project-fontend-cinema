import { useNavigate } from "react-router-dom";
import { image_300, unavailable } from "../../Api/config/DefaultConfig"
import "../ProductMovies/ProductMovies.css"
import MuiPlayArrowRoundedIcon from "@mui/icons-material/PlayArrowRounded";
import { styled } from "@mui/material/styles";
const PlayArrowRoundedIcon = styled(MuiPlayArrowRoundedIcon)(`

  &.MuiSvgIcon-root{
    color:#abb7c4 ;
  },  &.MuiSvgIcon-root:hover {
    color: #d13131 ;
  }
  
`);
const ProductMovies = ({
    poster_path,
    title,
    name,
    id,
    vote_average,
    release_date,
    first_air_date,
    mediaType,
    media_type,

}) => {
    console.log("poster_path", poster_path)
    const setVote = (vote) => {
        if (vote >= 8) {
            return "green"
        } else if (vote >= 6) {
            return "orange"
        } else {
            return "red"
        }
    }
    const navigate = useNavigate();
    const handOnClick = () => {
        navigate(`/Detail/${mediaType || media_type}/${id}`)
    }
    return (
        <>
            <div className="productMedia" onClick={handOnClick}>
                <span className={`tag ${setVote(vote_average)} vote_class`}>{Math.round(vote_average * 10) / 10}</span>
                <img src={poster_path ? `${image_300}/${poster_path}` : unavailable} alt="" />
                <div className="read__more">
                    <PlayArrowRoundedIcon
                        style={{
                            border: "2px solid #abb7c4",
                            borderRadius: "50px",
                            fontSize: "3.2rem",
                            cursor: "pointer",
                        }}
                        className="play-btn"
                    />
                </div>
                <div className="ProductDetails">
                    <h6>
                        {title || name}(
                        {(first_air_date || release_date || "-----").substring(0, 4)})
                    </h6>
                </div>
            </div>

        </>
    )
}
export default ProductMovies