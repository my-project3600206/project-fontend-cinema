
import { Link } from "react-router-dom"
import logo from "../../assets/images/hbo-max.svg"
import HomeIcon from '@mui/icons-material/Home';
import TheatersIcon from "@mui/icons-material/Theaters";
import LiveTvIcon from '@mui/icons-material/LiveTv';
import WhatshotIcon from '@mui/icons-material/Whatshot';
import DehazeIcon from '@mui/icons-material/Dehaze';
import $ from "jquery";
import "../Headers/Main.Header.css"

//change bg color of header when scroll 
$(function () {
    $(document).on("scroll", function () {
        var $nav = $(".navbar");
        $nav.toggleClass("scrolled", $(this).scrollTop() > $nav.height());
    });
});
const MainHeader = () => {
    return (
        <>
            <nav className="navbar navbar-expand navbar-light fixed-top">
                <Link className="nav-brand" to="/">
                    <div className="main-logo">
                        <img src={logo} alt="logo" width="60%" />
                    </div>
                </Link>
                
                <div className="collapse navbar-collapse d-flex justify-content-between" >
                    <ul className="navbar-nav me-auto mb-2 mb-lg-0">
                        <li className="nav-item active navbar_link">
                            <Link className="nav-link active" to="/">
                                <HomeIcon />
                                HOME
                            </Link>
                        </li>
                        <li className="nav-item  navbar_link">
                            <Link className="nav-link" to="/Treading">
                                <WhatshotIcon />
                                TREADING
                            </Link>
                        </li>
                        <li className="nav-item  navbar_link">
                            <Link className="nav-link " to="/all-Movies">
                                <TheatersIcon width="10px" />
                                MOVIES
                            </Link>
                        </li>
                        <li className="nav-item  navbar_link">
                            <Link className="nav-link " to="/all-Series">
                                <LiveTvIcon width="10px" />
                                TVSERIES
                            </Link>
                        </li>
                    </ul>
                </div>
                <div className="login" style={{ marginRight: "50px" }}>
                    <button className="btn-login">LOGIN</button>
                </div>
            </nav>
        </>
    )
}
export default MainHeader