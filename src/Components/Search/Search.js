import { useEffect } from "react";
import "../../Components/Search/Search.css"
import SearchIcon from '@mui/icons-material/Search';
const SearchFilm = ({setSearchFilm, searchFilm, setPage, fetchGetApiSearchFilm}) => {
    //hand onchange input search
    const handSearchOnChange = (e)=>{
        
        setSearchFilm(e.target.value);
        setPage(1);
    }
    const handSearchOnClick=()=>{
        fetchGetApiSearchFilm();
        setPage(1);
    }
    useEffect(()=>{
        fetchGetApiSearchFilm();
        return () => { };
    },[])
    return (
        <>
            <div className="search-term">
                <input value={searchFilm} type="text" placeholder="search..." className="form-control" onChange={handSearchOnChange}></input>
                <button className="btn btn-primary" onClick={handSearchOnClick}>Search</button>
            </div>
        </>
    )
}
export default SearchFilm