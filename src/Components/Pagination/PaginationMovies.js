import { Container, Grid } from "@mui/material";
import Pagination from '@mui/material/Pagination';
import Stack from '@mui/material/Stack';
import "../../Components/Pagination/PaginationMovies.css"
import { useNavigate } from "react-router-dom";


const PaginationMovies = ({setPage, numberPage, media, genreFilter, searchFilm, genreTitle, page}) =>{
    const navigation = useNavigate();
    const handlerPageOnchange = (currentPage)=>{
        console.log("currentPage", currentPage)
        console.log("genreTitle", genreTitle)
        
        if(searchFilm){
            navigation(`/${media}/${searchFilm}/page/${currentPage}`)
        }
        else if(genreFilter){
            console.log("test genreFilter",genreFilter)
            if(media === "movie"){
               {genreTitle ? navigation(`/movie/categories/${media}/${genreTitle.name}/page/${currentPage}`): navigation(`/movie/categories/${media}/page/${currentPage}`)};
            }else if(media==="series"){
                {genreTitle ? navigation(`/series/categories/${media}/${genreTitle.name}/page/${currentPage}`): navigation(`/series/categories/${media}/page/${currentPage}`)};
            }
        }
        else{
            navigation(`/${media}/page/${currentPage}`);
        }
        setPage(currentPage);
        
    }

    return (
        <Container>
            <Grid container mt={3} justifyContent={"center"}>
                <Grid item>
                    <Stack spacing={2}>
                        <Pagination  onChange={(e, value)=>handlerPageOnchange(value)} count={numberPage} variant="outlined" shape="rounded" color="primary" className="ul MuiPaginationItem-root MuiPaginationItem-ellipsis Mui-selected"/>
                    </Stack>
                </Grid>
            </Grid>
        </Container>
    )
}
export default PaginationMovies