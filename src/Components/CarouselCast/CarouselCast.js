import axios from "axios";
import React, { useEffect, useState } from "react";
import AliceCarousel from "react-alice-carousel";
import "react-alice-carousel/lib/alice-carousel.css";
import { image_300, noPicture } from "../../Api/config/DefaultConfig";
import "../../Components/CarouselCast/CarouselCast.css"

const Cast = ({ mediaType, id }) => {
    const [credits, setCredits] = useState();

    const responsive = {
        0: {
            items: 1,
        },
        380: {
            items: 1,
        },
        512: {
            items: 2,
        },
        665: {
            items: 3,
        },
        767: {
            items: 3,
        },
        870: {
            items: 4,
        },
        1024: {
            items: 6,
        },
        1265: {
            items: 7,
        },
    };

    const items = credits?.map((characterInfo) => {
        return (
            <div className="carousel_cast">
                <img
                    src={characterInfo.profile_path ? `${image_300}/${characterInfo.profile_path}` : noPicture}
                    alt=""
                    className="cast_img"
                    //onDragStart={handleDragStart}
                />
                <div className="cast_details">
                    <h6 className="cast_name">{characterInfo.original_name}</h6>
                    <h6 className="character">{characterInfo.character}</h6>
                </div>
            </div>
        );
    });

    //get character of current movie
    const fetchCredits = async () => {
        try {
            const { data } = await axios.get(` 
            https://api.themoviedb.org/3/${mediaType}/${id}/credits?api_key=39f5897aa2b8f37692fc06e61504587d`);
            const dataSliceCast = data.cast;
            setCredits(dataSliceCast);
            console.log("dataCast",data);

        } catch (error) {
            console.error(error);
        }
    };
    console.log('carousel of detaill page')

    useEffect(() => {
        fetchCredits();
    }, [mediaType, id]);

    return (
        <AliceCarousel
            infinite
            autoPlay
            disableButtonsControls
            disableDotsControls
            mouseTracking
            items={items}
            responsive={responsive}
        />
    );
};

export default Cast;
