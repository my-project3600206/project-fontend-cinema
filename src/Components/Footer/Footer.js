import { Link } from "react-router-dom";
import logo from "../../assets/images/hbo-max.svg"
import "../Footer/Footer.css"

const Footer = () => {
    return (
        <>
            <div className="container-fluid">
                <div className="row footer">
                    <div className="col-sm-3">
                        <div className="colum">
                            <div className="footer-logo">
                                <img src={logo} alt="logo2" />
                            </div>
                            <ul className="colum-item" style={{marginTop:"15px"}}>
                                <li className="link-item">
                                    <Link to="#">Cinemy Movies and Tv Series</Link>
                                </li>
                                <li className="link-item">
                                    <Link to="#">Nakuru, Kenya</Link>
                                </li>
                                <li className="link-item">
                                    <Link to="#">call Us: (+254) 78728340</Link>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div className="col-sm-3">
                        <div className="colum">
                            <div className="footer-title">
                                <h6>Resources</h6>
                            </div>
                            <ul className="colum-item">
                                <li className="link-item">
                                    <Link to="#">About CinemyPlex</Link>
                                </li>
                                <li className="link-item">
                                    <Link to="#">Contact Us</Link>
                                </li>
                                <li className="link-item">
                                    <Link to="#">Blog</Link>
                                </li>
                                <li className="link-item">
                                    <Link to="#">Help Center</Link>
                                </li>

                            </ul>
                        </div>

                    </div>
                    <div className="col-sm-3">
                        <div className="footer-title">
                            <h6>Legal</h6>
                        </div>
                        <div className="colum">
                            <ul className="colum-item">
                                <li className="link-item">
                                    <Link to="#">Terms of Use</Link>
                                </li>
                                <li className="link-item">
                                    <Link to="#">Privacy Policy</Link>
                                </li>
                                <li className="link-item">
                                    <Link to="#">Security</Link>
                                </li>

                            </ul>
                        </div>

                    </div>
                    <div className="col-sm-3">
                        <div className="footer-title">
                            <h6>Newsletter</h6>
                        </div>
                        <div className="colum">
                            <ul className="colum-item">
                                <li className="link-item">
                                    <Link to="#">Subscribe to our newsletter system now to get latest news from us</Link>
                                </li>
                            </ul>
                            <div className="inp-email">
                                <input type="email" placeholder="Enter your email" />
                            </div>
                            <div className="btn-sub">
                                <button className="footer-email-submit">SUBSCRIBE NOW</button>

                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </>
    )
}
export default Footer