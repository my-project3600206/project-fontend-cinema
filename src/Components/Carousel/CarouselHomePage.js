import AliceCarousel from 'react-alice-carousel';
import "react-alice-carousel/lib/alice-carousel.css";
import "../Carousel/CarouselHomePage.css"
import axios from 'axios';
import { useEffect, useState } from 'react';



const CarouselHomePage = () => {

    const [allContent, setAllContent] = useState([])
    const items = allContent.map((item) => (
        <div
            key={item.id}
            className='nav_main'
            style={{
                backgroundImage: `url( https://www.themoviedb.org/t/p/w1920_and_h800_multi_faces/${item.backdrop_path})`,
            }}
        >
            <div className="nav-info">
                <h3>{item.title || item.name}</h3>
                <h5 style={{ color: "#abb7c4" }}>
                    {item.media_type === "tv" ? "Tv Series" : "Movie"}
                </h5>

                <p>{item.overview}</p>
                <div className="btn-learn">
                    <button >
                        LEARN MORE
                    </button>
                </div>
            </div>

        </div>
    ))
    const fetchApiGetMovie = async () => {
        try {
            const { data } = await axios.get(
                "https://api.themoviedb.org/3/trending/all/day?api_key=39f5897aa2b8f37692fc06e61504587d",
            );
            const allData = data.results;
            const filter = allData.slice(0, 10);
            const res = filter.reverse();
            setAllContent(res);
            console.log("res", res)
        }
        catch (error) {
            console.error(error);
        }

    }
    useEffect(() => {
        fetchApiGetMovie();
    }, [])
    return (
        <>
            <AliceCarousel
                infinite
                autoPlay
                disableButtonsControls
                disableDotsControls
                mouseTracking
                autoPlayInterval={1500}
                items={items}
            />
        </>
    )
}
export default CarouselHomePage