import axios from "axios";
import { useEffect, useState } from "react";
import { Link, useNavigate } from "react-router-dom";
import "../../Components/FilterGenre/FilterGenre.css";

const FilterGenre = ({ media, genreTitle, genreFilter, setPage, setSearchFilm}) => {
    console.log("media:", media)
    //get all genres of type movies - media
    const [ResGenres, setGenres] = useState([])
    const navigation = useNavigate();
    const fetchApiGetGenres = async () => {
        try {
            if (media) {
                const { data } = await axios.get(
                    `https://api.themoviedb.org/3/genre/${media}/list?api_key=39f5897aa2b8f37692fc06e61504587d&language=en-US`
                );
                console.log("ResGenres", data.genres)
                setGenres(data.genres);
            }
        } catch (error) {
            console.error(error);
        }
    };
    const handleDropdown = (e) => {
        let dropList = document.querySelector("#dropdownMenuLink ~.dropdown-menu")
        dropList.classList.toggle("show")
    }
    const handClickFilter = (genre)=>{
        genreTitle(genre);
        genreFilter(genre);
        setPage(1);
       
        if(media === "movie"){
            navigation(`/movie/categories/${media}/${genre.name}/page/1`);
        }else if(media==="tv"){
            navigation(`/series/categories/${media}/${genre.name}/page/1`);
        }else if(!media){
            navigation(`/all-Series`);
        }
        let dropList = document.querySelector("#dropdownMenuLink ~.dropdown-menu")
        dropList.classList.toggle("show");
        setSearchFilm("");
       
    }
    useEffect(() => {
        fetchApiGetGenres();
        
    }, [])
    return (
        <>
            <div className="dropdown">
                <Link to="#" id="dropdownMenuLink" onClick={handleDropdown} className="btn btn-secondary dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                    Filter By:
                </Link>
                <div className="dropdown-menu" aria-labelledby="dropdownMenuLink" >
                    <div className="title-genres">Categories :</div>
                    <div className="genres-content">
                        <button className="btn btn-secondary" onClick={handClickFilter}> {media === "movie" ? "All Movies" : "All Tv Series"}  </button>
                        {ResGenres && (ResGenres.map((genre)=>(
                            <button  key={genre.id} className="btn btn-secondary dropGenre-item" onClick={()=>handClickFilter(genre)}>{genre.name}</button>
                        )))}
                    </div>
                </div>
            </div>
        </>
    )
}
export default FilterGenre