import YouTubeIcon from '@mui/icons-material/YouTube';
import "../VideoTrailer/VideoTrailer.css"
import $ from "jquery";
import CloseIcon from '@mui/icons-material/Close';



const VideoTrailer = ({ keyTrailer, title }) => {

    const OpenModalTrailer = () => {
        $(".modal").show();
    }
    const CloseModalTrailer = () => {
        $(".modal").hide();
    }
    const styleWatch = {
        width: "160px", height: "40px", background: "rgba(60, 63, 63, 0.1)",
        color: "#fff", border: "1px solid #fff", borderRadius: "5px",
    }

    return (
        <>
            <div className="Trailer">
                <button onClick={OpenModalTrailer} className='btn-video' style={styleWatch}><YouTubeIcon style={{ color: "red", fontSize: "27px" }} /> Watch Video</button>
            </div>
            <div className="modal" tabIndex={"-1"}>
                <div className="modal-dialog">
                    <div className="modal-content">
                        <div className="modal-header">
                            <h5 className="modal-title">{title} Official Trailer</h5>
                            <button type="button" onClick={CloseModalTrailer} ><CloseIcon /></button>
                        </div>
                        <div className="modal-body">
                            <iframe
                                title= {title}
                                width= {"800px"}
                                height={"400px"}
                                src={`https://www.youtube-nocookie.com/embed/${keyTrailer}?autoplay=0&amp;origin=http%3A%2F%2Fwww.themoviedb.org&amp;hl=en&amp;modestbranding=1&amp;fs=1&amp;autohide=1`}
                            >
                            </iframe>

                        </div>
                    </div>
                </div>
            </div>
        </>
    )
}
export default VideoTrailer